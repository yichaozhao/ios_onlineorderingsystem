//
//  NotificationViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 15/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Promotion"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didClickMenuButton(sender: AnyObject) {
                print("Click Button")
        self.view!.endEditing(true) // Dismiss Keyboard
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
