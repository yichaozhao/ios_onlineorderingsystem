//
//  SettingViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 18/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var table: UITableView!
    
    private var datasource = [String]()
    
    @IBAction func didSelectedSegmentElement(sender: UISegmentedControl) {
//        self.datasource = NSMutableArray()
        assignDataSource(self.segment.selectedSegmentIndex)
        self.table.reloadData()
    }
    
    func assignDataSource(selectedIndex:Int){
        var temp = selectedIndex + 1
        var path = NSBundle.mainBundle().pathForResource("setting\(temp)", ofType: "plist")
        var array = NSArray(contentsOfFile: path!)!
        self.datasource = array as! [String]
        print("datasource = \(self.datasource)")
        
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.table.delegate = self
        self.table.dataSource = self
        
        assignDataSource(0)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.textLabel!.text = self.datasource[indexPath.row]
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var defaults = NSUserDefaults.standardUserDefaults()
        if(self.segment.selectedSegmentIndex == 0) {
            var content = self.datasource[indexPath.row]
            defaults.setValue(content, forKey: "label")
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
