//
//  DataLoader.h
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 17/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

#ifndef DataLoader_h
#define DataLoader_h
@interface DataLoader : NSObject 
@property NSArray *titleArray;
@property NSArray *leftArray;
@property NSArray *rightArray;
@end


#endif /* DataLoader_h */
