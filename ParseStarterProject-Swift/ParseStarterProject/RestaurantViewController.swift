//
//  RestaurantViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 15/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

private let reuseIdentifier = "mainCell"
class RestaurantViewController: UIViewController, dropdownDelegate,UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    var datasource: NSArray?
    var temp = 0
    @IBOutlet weak var collectionView: UICollectionView!
    var firstItem: UIBarButtonItem?
    var secondItem: UIBarButtonItem?
    var thirdItem: UIBarButtonItem?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Add Hamburger menu
        var image =        UIImage(named: "icon_category_-1")
        var menuItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Done, target: self, action: Selector("didClickButton:"))
        //        menuItem.tintColor = UIColor.redColor()
        
        var searchBar = UISearchBar(frame: CGRectMake(0, 0.0, 200, 40.0))
        searchBar.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        var searchBarView = UIView(frame: CGRectMake(0.0, 0.0, 200, 40.0))
        searchBar.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        searchBar.placeholder = "Search VENICA"
        searchBarView.addSubview(searchBar)
        searchBar.backgroundColor = UIColor.redColor()
        searchBarView.backgroundColor = UIColor.redColor()
        
        var image2 =        UIImage(named: "icon_detail_address")!
        var menuItem2 = UIBarButtonItem(image: image2, style: UIBarButtonItemStyle.Done, target: self, action: nil)
        self.navigationItem.leftBarButtonItem = menuItem
        self.navigationItem.rightBarButtonItem = menuItem2
        self.navigationItem.titleView = searchBarView
        
        
        // Collectio View
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView!.registerNib(UINib(nibName: "MyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        var layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSizeMake(320, 240)
        
        self.collectionView!.backgroundColor = UIColor.whiteColor()
        self.collectionView!.collectionViewLayout = layout
        
        // Prepare Data
        var model = DataModel()
        self.datasource  = model.loadDataFromPlist()
        //        self.datasource = model.getDataWithArray(data)
        
        
        
        /*
        //self.title = "Restaurant"
        //self.view.backgroundColor =  UIColor(colorLiteralRed: 255/255, green: 10/255, blue: 10/255, alpha: 0.85)
        
        var menuItem = UIBarButtonItem(image: UIImage(named: "icon_category_-1")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), style: UIBarButtonItemStyle.Done, target: self, action: Selector("didClickButton:"))
        
        
        var loader = DataLoader()
        
        
        var dropdownList : DropdownMenu = DropdownMenu(dropdownWithButtonTitles: loader.titleArray, andLeftListArray: loader.leftArray, andRightListArray: loader.rightArray)
        
        dropdownList.delegate = self
        self.view.addSubview(dropdownList.view)
        */
        /*
        var first = NavItem.makeItem() as! NavItem
        first.setText("Area")
        var second = NavItem.makeItem() as! NavItem
        second.setText("Food")
        var third = NavItem.makeItem() as! NavItem
        third.setText("Status")
        first.backgroundColor = UIColor.clearColor()
        second.backgroundColor = UIColor.clearColor()
        third.backgroundColor = UIColor.clearColor()
        
        first.addTarget(self, action: Selector("didClickFirstMenu:"))
        second.addTarget(self, action: Selector("didClickSecondMenu:"))
        third.addTarget(self, action: Selector("didClickThirdMenu:"))
        
        firstItem = UIBarButtonItem(customView: first)
        secondItem = UIBarButtonItem(customView: second)
        thirdItem = UIBarButtonItem(customView: third)
        
        
        
        
        //        UIImageView
        //        var firstItem = UIBarButtonItem(customView: NavItem.makeItem())
        //        self.navigationItem.leftBarButtonItems
        */
        //        self.navigationItem.leftBarButtonItems = [menuItem]
        self.title = "Restaurant"
        // Do any additional setup after loading the view.
    }
    
    func didClickButton(sender: AnyObject){
        print("Click Button")
        self.view!.endEditing(true) // Dismiss Keyboard
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
        
    }
    
    
    func didClickFirstMenu(sender: AnyObject){
        print("Click First Button")
        
        createPopver()
    }
    
    func didClickSecondMenu(sender: AnyObject){
        print("Click Second Button")
        createPopver()
        
    }
    
    func didClickThirdMenu(sender: AnyObject){
        print("Click Third Button")
        createPopver()
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    
    func createPopver(){
        /*
        var pvc = PopViewController()
        var pop = UIPopoverController(contentViewController: pvc)
        pop.presentPopoverFromBarButtonItem(self.firstItem!, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        */
    }
    
    func dropdownSelectedLeftIndex(left: String!, rightIndex right: String!) {
        print("left = \(left), right = \(right)")
    }
    
    // MARK: UICollectionViewDataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.datasource!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : MyCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MyCollectionViewCell
        
        // Configure the cell
        cell.showUIWithModel(self.datasource![indexPath.section] as! DataModel)
        print("count = \(indexPath.section)")
        print("show model = \(self.datasource![indexPath.section])")
        return cell
    }
    
    // Unwind
    @IBAction func unwindByCancelSelection(segue: UIStoryboardSegue) {
        
    }
    
    override func viewDidAppear(animated: Bool) {
        var defaults = NSUserDefaults()
        var temp = defaults.valueForKey("label")
        
        //        self.
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // Cache Data
//        var defaults = NSUserDefaults.standardUserDefaults()
//        defaults.setObject(indexPath.row, forKey: "index")
        //        defaults.setValue(, forKey: "model")
        //        var defaults = NSUserDefaults.setValue(indexPath.row, forKey: "index")
        temp = indexPath.section
        performSegueWithIdentifier("showDetail", sender: self)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Get the new view controller using segue.destinationViewController.
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "showDetail"){
        var destViewController: RestaurantDetailViewController = segue.destinationViewController as! RestaurantDetailViewController
        destViewController.model = self.datasource![temp] as! DataModel
        print("selection section = \(temp)")
//        destViewController.model =
        
        }
    }
    
    @IBAction func unwindByBack2Button(segue: UIStoryboardSegue) {
            
    }
}

