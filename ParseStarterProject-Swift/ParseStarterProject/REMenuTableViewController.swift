//
//  REMenuTableViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 15/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

class REMenuTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let backgroundView = UIView(frame: CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height))
//        backgroundView.backgroundColor = UIColor(colorLiteralRed: 203/225.0, green: 33/255.0, blue: 45/255.0, alpha: 0.8) //255, 97, 89
        
        self.tableView.backgroundView = backgroundView
        self.tableView.separatorColor = UIColor(colorLiteralRed: 150/225.0, green: 161/225.0, blue: 177/255.0, alpha: 1.0)
        self.tableView.opaque = false
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.tableHeaderView = self.setHeaderView()


        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0
        }
        return 34
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 54
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(section == 0){
            return nil
        }
        
        print("section = \(section)")
        
        var view : UIView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 34))
        view.backgroundColor = UIColor(colorLiteralRed: 167/255.0, green: 167/255.0, blue: 167/255, alpha: 0.6)
        
        var label:UILabel = UILabel(frame: CGRectMake(10, 8, 0, 0))
        label.text = "Test2"
        label.font = UIFont.systemFontOfSize(15)

        label.textColor = UIColor.redColor()
        label.backgroundColor = UIColor.clearColor()
        label.sizeToFit()
        view.addSubview(label)
        return view
    }
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel!.textColor = UIColor.blackColor()
        cell.textLabel!.font = UIFont(name: "HelveticaNeue", size: 17)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cellIdentifier = "cell"
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        if(indexPath.section == 0){
            var titles = ["Restaurants", "My Orders", "My Location", "Show Room", "Promotions"]

            cell!.textLabel!.text = titles[indexPath.row]
            cell!.textLabel!.textColor = UIColor.blackColor()

        }
        /*
        else{
        var titles = ["待定", "待定"]
        cell!.textLabel!.text = titles[indexPath.row]
        }*/
        return cell!
        
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("indexpath = \(indexPath)")   // index path = location of clicking
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        // Get Navigation Controller
        var navigationController : REContentViewController = self.storyboard!.instantiateViewControllerWithIdentifier("contentViewController") as! REContentViewController
        
    
     
//        if indexPath.section == 0 && indexPath.row == 0 {
            // Storyboard -> ViewController
        
        switch (indexPath.section, indexPath.row){
            case (0, 0):
                
                navigationController.navigationBar.backgroundColor = UIColor.redColor()

            
            var restViewContoller : RestaurantViewController = self.storyboard!.instantiateViewControllerWithIdentifier("restaurantView") as! RestaurantViewController
            navigationController.viewControllers = [restViewContoller]

            
            case (0, 1):
            var orderViewContoller : OrderViewController = self.storyboard!.instantiateViewControllerWithIdentifier("orderView") as! OrderViewController
            navigationController.viewControllers = [orderViewContoller]

            case (0, 2):
            var locationViewController : LocationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("locationView") as! LocationViewController
            navigationController.viewControllers = [locationViewController]

            case (0, 3):
            var showView : ShowViewController = self.storyboard!.instantiateViewControllerWithIdentifier("showroomView") as! ShowViewController
            navigationController.viewControllers = [showView]
            case (0, 4):
                var promotionView : NotificationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("promotionView") as! NotificationViewController
                navigationController.viewControllers = [promotionView]
            default:
                print("default")
            
        }
                   // Reset ContentViewController
        self.frostedViewController.contentViewController = navigationController;
        self.frostedViewController.hideMenuViewController()
    }

    
    
    // Functions
    
    private func setHeaderView()->UIView{
        var view: UIView = UIView(frame: CGRectMake(0, 0, 0, 184))

        var imageView : UIImageView = UIImageView(frame: CGRectMake(0, 40, 100, 100))
        imageView.autoresizingMask = [UIViewAutoresizing.FlexibleLeftMargin, UIViewAutoresizing.FlexibleRightMargin]
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 50.0
        
        
        imageView.image = UIImage(named: "icon.jpg")
        imageView.layer.borderColor = UIColor.whiteColor().CGColor
        imageView.layer.borderWidth = 3.0;
        imageView.layer.rasterizationScale = UIScreen.mainScreen().scale
        imageView.layer.shouldRasterize = true;
        imageView.clipsToBounds = true;

        var label : UILabel = UILabel(frame: CGRectMake(0, 150, 0, 24))
        
   
            label.text = "Login"
        
   /*
        var uiButtom = UIButton(type: UIButtonType.Custom)
        uiButtom.setImage(UIImage(named: "icon_mine_myAccount_username@2x.png"), forState: UIControlState.Normal)
        uiButtom.frame = CGRectMake(0, 40, 100, 100)
*/
        label.font = UIFont(name: "HelveticaNeue", size: 21)
        label.backgroundColor = UIColor.clearColor()
        label.textColor = UIColor(colorLiteralRed: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        label.sizeToFit()
        label.autoresizingMask = [UIViewAutoresizing.FlexibleLeftMargin, UIViewAutoresizing.FlexibleRightMargin]
        
        let singleTap = UITapGestureRecognizer(target: self, action: Selector("tapDetected"))
        singleTap.numberOfTapsRequired = 1
        
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(singleTap)
        
        view.addSubview(imageView)
        view.addSubview(label)
        view.backgroundColor = UIColor.redColor()
        // Bind to CLickUIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, nextY)] autorelease];
        view.targetForAction("didClickHeader:", withSender: self)
//        [headerView addTarget:self action:@selector(myEvent:) forControlEvents:UIEve];
        return view
    }
    
    func tapDetected(){
//        let loginViewController : LoginViewController = self.storyboard!.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
        
        // Redirect
//        self.performSegueWithIdentifier("loginSegue", sender: self)
    }
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func unwindByBackButton(segue: UIStoryboardSegue) {
        
    }
    
  

}
