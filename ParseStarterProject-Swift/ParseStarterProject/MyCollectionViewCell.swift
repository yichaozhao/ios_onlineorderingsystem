//
//  MyCollectionViewCell.swift
//  CollectionViewTest
//
//  Created by Yichao Zhao on 18/10/2015.
//  Copyright © 2015 Yichao Zhao. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblOrder: UILabel!
    @IBOutlet weak var lblRoad: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func showUIWithModel(model: DataModel){
        self.lblName.text = model.name!
        self.lblLocation.text = model.suburb!
        self.lblDistance.text = "\(model.distance!) km"
        self.lblOrder.text = "\(model.orderNo!) orders"
        self.lblRoad.text = model.road!
        var image = UIImage(named: model.logo!)
        self.image.image = image!
        
    }

}
