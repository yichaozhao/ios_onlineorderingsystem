//
//  RestaurantDetailViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 18/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

class RestaurantDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var navigationBar: UINavigationBar!
    var model : DataModel?
    var datasource : [String]  = [String]()
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("self title = \(        self.navigationItem.title)")
navItem.title = self.model!.name!
//        self.navigationBar.title = self.model!.name!
//        self.navigationBar.item =
        // Do any additional setup after loading the view.
        // TableView
        self.table.delegate = self
        self.table.dataSource = self
        
        self.img.image = UIImage(named: self.model!.logo!)
        self.lblAddress.text = "\(self.model!.road!), \(self.model!.suburb!)"
        /*
        var defaults = NSUserDefaults.standardUserDefaults()
        if let _model =         defaults.valueForKey("model"){
            self.model = _model as! DataModel
        }
*/
        print("model = \(self.model)")

        assignDataSource()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func assignDataSource(){
//        var temp = selectedIndex + 1
        var path = NSBundle.mainBundle().pathForResource("menu", ofType: "plist")
        var array = NSArray(contentsOfFile: path!)!
        self.datasource = array as! [String]
        print("datasource = \(self.datasource)")
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK - TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.textLabel!.text = self.datasource[indexPath.row]
        return cell!
    }
    
   

}
