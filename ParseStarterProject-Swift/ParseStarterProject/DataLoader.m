//
//  DataLoader.m
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 17/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataLoader.h"

@implementation DataLoader


- (NSArray *)titleArray{
    return @[@"附近", @"菜品"];
}

- (NSArray *)leftArray{
    NSArray *One_leftArray = @[@"附近", @"熱門商區", @"香洲區", @"斗門區", @"金灣區"];
    NSArray *Two_leftArray = [[NSArray alloc] init];
    return [[NSArray alloc] initWithObjects:One_leftArray, Two_leftArray, nil];
}


//左边列表可为空，则为单下拉菜单，可以根据需要传参
-(NSArray *)rightArray{
   
    //    NSArray *R_leftArray = @[@"Test1", @"Test2"];
    NSArray *F_rightArray = @[
                              @[
                                  @{@"title":@"500米"},
                                  @{@"title":@"1000米"},
                                  @{@"title":@"2000米"},
                                  @{@"title":@"5000米"}
                                  ] ,
                              @[
                                  @{@"title":@"全部商區"},
                                  @{@"title":@"拱北"},
                                  @{@"title":@"香洲"},
                                  @{@"title":@"吉大"},
                                  @{@"title":@"華髮商都"},
                                  @{@"title":@"富華里"},
                                  @{@"title":@"揚名廣場"},
                                  @{@"title":@"摩爾廣場"},
                                  @{@"title":@"井岸鎮"},
                                  @{@"title":@"紅旗鎮"},
                                  @{@"title":@"三灶鎮"},
                                  ],
                              @[
                                  @{@"title":@"全部香洲區"},
                                  @{@"title":@"拱北"},
                                  @{@"title":@"香洲"},
                                  @{@"title":@"吉大"},
                                  @{@"title":@"華髮商都"},
                                  @{@"title":@"富華里"},
                                  @{@"title":@"揚名廣場"},
                                  @{@"title":@"摩爾廣場"},
                                  ],
                              @[
                                  @{@"title":@"全部斗門區"},
                                  @{@"title":@"井岸鎮"},
                                  ],
                              @[
                                  @{@"title":@"全部金灣區"},
                                  @{@"title":@"紅旗鎮"},
                                  @{@"title":@"三灶鎮"},
                                  ]
                              ];
    
    NSArray *S_rightArray = @[
                              @[
                                  @{@"title":@"one"},
                                  @{@"title":@"two"},
                                  @{@"title":@"three"}
                                  ] ,
                              @[
                                  @{@"title":@"four"}
                                  ]
                              ];
    
    return [[NSArray alloc] initWithObjects:F_rightArray, S_rightArray, nil];
    
    
}



@end

