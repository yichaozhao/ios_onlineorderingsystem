//
//  NavItem.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 17/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

class NavItem: UIView {

    @IBOutlet private weak var btnButton: UIButton!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    @IBOutlet weak var lblText: UILabel!
    class func makeItem() -> UIView{
        return (NSBundle.mainBundle().loadNibNamed("NavItem", owner: self, options: nil)).first as! UIView
    }
    
    func setText(text:String){
        self.lblText.text = text
    }
    
    func addTarget(target: AnyObject, action: Selector)
    {
        self.btnButton.addTarget(target, action: action, forControlEvents: UIControlEvents.TouchUpInside)
    }

}
