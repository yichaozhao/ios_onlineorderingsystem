//
//  OrdeModel.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 18/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import Foundation
class OrdeModel: CustomStringConvertible{
    var name: String?
    var price: Double?
    var logo: String?
    var datasource = [AnyObject]()

    // return list of Model
    func loadDataFromPlist() -> NSArray{
        var fileaddress = NSBundle.mainBundle().pathForResource("restaurant", ofType: "plist")
        var plistArray = NSArray(contentsOfFile: fileaddress!)
        var dataArray = self.getDataWithArray(plistArray!)
        return dataArray
    }

    func makeModelFromDictionary(dic: NSDictionary)->OrdeModel{
        self.name = dic.objectForKey("name") as! String
//        self.suburb = dic.objectForKey("suburb") as! String
//        self.road = dic.objectForKey("road") as! String
//        self.orderNo = dic.objectForKey("order_no") as! Int
        self.logo = dic.objectForKey("logo") as! String
        self.price = dic.objectForKey("price")!.doubleValue
//        self.distance = dic.objectForKey("distance") as! Int
        return self
    }
    
    
    // Data = return value of loadDataFromPlist
    // Rturn array of Model
    func getDataWithArray(data: NSArray) -> NSArray{
        var arr = NSMutableArray()
        
        for var i = 0; i < 3; i++ {
            var model = OrdeModel().makeModelFromDictionary(data.objectAtIndex(i) as! NSDictionary)
            arr.addObject(model)
            print("model = \(model)")
        }
        
        return arr
    }
    
    var description : String{
        return "Order: name = \(name!), logo = \(logo!), price = \(price!))"
    }

}