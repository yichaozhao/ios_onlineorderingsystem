//
//  OrderViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 15/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

private let reuseIdentifier = "mainCell"
class OrderViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate  {

        var datasource: NSArray?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var btnOrder: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Order"
        // Do any additional setup after loading the view.
        self.btnOrder.backgroundColor = UIColor.redColor()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        // Set Navation Item
        // Add Hamburger menu
        var image =        UIImage(named: "icon_category_-1")
        var menuItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Done, target: self, action: Selector("didClickButton:"))
        
         self.navigationItem.leftBarButtonItem = menuItem
        
        
        // CollectionView
        self.collectionView!.registerNib(UINib(nibName: "OrderCollectionCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        var layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSizeMake(320, 240)
        self.collectionView!.backgroundColor = UIColor.whiteColor()
        self.collectionView!.collectionViewLayout = layout

        // Prepare Data
        var model = OrdeModel()
        self.datasource  = model.loadDataFromPlist()
        
        self.lblCost.text = "$ \(self.getCost())"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getCost() -> Double {
        var ret = 0.0
        for var i = 0; i < self.datasource!.count; i++ {
            var data = (self.datasource![i] as! OrdeModel)
            ret += data.price!
        }
        return ret
    }
    
    func didClickButton(sender: AnyObject){
        print("Click Button")
        self.view!.endEditing(true) // Dismiss Keyboard
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
        
    }

 
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : OrderCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! OrderCollectionCell
        
        // Configure the cell
        cell.showUIWithModel(self.datasource![indexPath.section] as! OrdeModel)
        print("count = \(indexPath.section)")
        print("show model = \(self.datasource![indexPath.section])")
        return cell
    }

    // MARK: UICollectionViewDataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.datasource!.count
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
