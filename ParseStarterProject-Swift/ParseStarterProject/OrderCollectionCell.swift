//
//  OrderCollectionCell.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 18/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import UIKit

class OrderCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func showUIWithModel(model: OrdeModel){
        
        self.lblName.text = model.name!
        self.lblPrice.text = "$\(model.price!)"
        self.imgLogo.image = UIImage(named: model.logo!)
    }


}
