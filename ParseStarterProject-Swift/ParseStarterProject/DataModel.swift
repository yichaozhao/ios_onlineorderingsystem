//
//  DataModel.swift
//  ParseStarterProject-Swift
//
//  Created by Yichao Zhao on 18/10/2015.
//  Copyright © 2015 Yichao. All rights reserved.
//

import Foundation

class DataModel : CustomStringConvertible {

     var name: String?
     var suburb: String?
     var road: String?
     var orderNo: Int?
     var logo: String?
     var distance: Int?
    
    var datasource = [AnyObject]()
    
    
    // return list of Model
    func loadDataFromPlist() -> NSArray{
        var fileaddress = NSBundle.mainBundle().pathForResource("restaurant", ofType: "plist")
        var plistArray = NSArray(contentsOfFile: fileaddress!)
        var dataArray = self.getDataWithArray(plistArray!)
        return dataArray
    }
    
    func makeModelFromDictionary(dic: NSDictionary)->DataModel{
        self.name = dic.objectForKey("name") as! String
        self.suburb = dic.objectForKey("suburb") as! String
        self.road = dic.objectForKey("road") as! String
        self.orderNo = dic.objectForKey("order_no") as! Int
        self.logo = dic.objectForKey("logo") as! String
        self.distance = dic.objectForKey("distance") as! Int
        return self
    }
    
    // Data = return value of loadDataFromPlist
    // Rturn array of Model
    func getDataWithArray(data: NSArray) -> NSArray{
        var arr = NSMutableArray()
        
        for var i = 0; i < data.count; i++ {
            var model = DataModel().makeModelFromDictionary(data.objectAtIndex(i) as! NSDictionary)
            arr.addObject(model)
            print("model = \(model)")
        }
        
        return arr
    }
    
    var description : String{
        return "DataModel: name = \(name!), suburb = \(suburb!), road = \(road!), orderNo = \(orderNo!), logo = \(logo!), distance = \(distance!)"
    }
    
}